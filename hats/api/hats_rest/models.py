from django.db import models

# Create your models here.


class LocationVO(models.Model):
    shelf_number = models.PositiveSmallIntegerField(null=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=255, unique=True)


class Hat(models.Model):
    color = models.CharField(max_length=255)
    fabric = models.CharField(max_length=255)
    style_name = models.CharField(max_length=255)
    picture_url = models.URLField(max_length=255, null=True)

    location = models.ForeignKey(LocationVO, related_name="hats", ond_delete=models.CASCADE)

    def __str__(self):
        return self.style_name