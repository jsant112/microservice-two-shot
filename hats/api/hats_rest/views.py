from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hat, LocationVO 
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
        "shelf_number",
        "section_number",
    ]


class HatsDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "color",
        "fabric",
        "style_name",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "picture_url",
        "id",
    ]


@require_http_methods({"GET", "POST"})
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hat = Hat.objects.filter(location_vo_id=location_vo_id)
        else:
            hat = Hat.objects.all()
            return JsonResponse(
                {"hats": hat},
                encoder=HatsListEncoder,
            )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location id is invalid"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods({"GET", "DELETE", "PUT"})
def api_show_hat(request, pk):
    if request.method == "GET":
        try:
            count, _ = Hat.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatsDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)

            props = ["color", "fabric", "style_name", "picture_url", "location", "id"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatsDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



